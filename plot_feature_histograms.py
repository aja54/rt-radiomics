"""
This script reads in the csv file that was saved during the feature selection process of training and then
plots histograms representing how many times specific features were chosen.
"""

import os
import csv
from glob import glob

import matplotlib.pyplot as plt
import numpy as np

file_formats = r'.\RadiomicFeatures\*\feature_selections.csv'


for num, file in enumerate(glob(file_formats)):
    if 'EID' in file or 'PCD' in file or 'IPECS' in file:
        if 'EID' in file:
            id_str = 'EID'
            word_shift = -5
        elif 'PCD' in file:
            id_str = 'PCD'
            word_shift = 0
        else:
            id_str = 'Material Decomposition'
            word_shift = -5

        chosen_features = list()
        counts = list()
        with open(file, 'r', newline='') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            for features in reader:
                for feature in features:
                    if feature not in chosen_features:
                        chosen_features.append(feature)
                        counts.append(1)
                    else:
                        counts[chosen_features.index(feature)] += 1

        folder = os.path.split(file)[0]
        hist_vals = zip(counts, chosen_features)
        hist_vals = sorted(hist_vals, reverse=True)

        plotted_counts = [x for x, y in hist_vals]  # if x > 2]
        plotted_features = [y for x, y in hist_vals]  # if x > 2]
        plotted_counts = plotted_counts[:11]
        plotted_features = plotted_features[:11]
        if plotted_counts[0] > 99:
            word_shift = -4
        else:
            word_shift = 0

        plt.figure(figsize=(12, 6))
        title = file.split('\\')[-2]
        x_locs = np.linspace(1, len(plotted_counts), len(plotted_counts), dtype=np.int8)
        plt.barh(x_locs, plotted_counts, height=0.7, tick_label=plotted_features)
        plt.xticks([])
        plt.yticks(ha='right', fontsize=15)
        for x, y in zip(x_locs, plotted_counts):
            plt.text(y+word_shift, x, y, fontsize=20, ha='center', va='center', backgroundcolor='w')

        plt.title(id_str, fontsize=20)
        axes = plt.gca()
        axes.invert_yaxis()
        plt.ylabel('Features', fontsize=14)
        plt.xlabel(f'Instance count (N out of 250)', fontsize=14)
        plt.tight_layout()

        folder = os.path.split(file)[0]
        filename = f'.\\Figures\\feature_selection_histogram_{id_str}.png'
        plt.savefig(filename, transparent=True, dpi=300)
        print(id_str + f':{len(chosen_features)}')


