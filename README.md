# RT Radiomics

This is the code used for the Radiomic analysis of Rag2 mouse models both with and without lymphocyte production. Based on the following paper:

Allphin AJ, Mowery YM, Lafata KJ, Clark DP, Bassil AM, Castillo R, Odhiambo D, Holbrook MD, Ghaghada KB, Badea CT. Photon Counting CT and Radiomic Analysis Enables Differentiation of Tumors Based on Lymphocyte Burden. Tomography. 2022; 8(2):740-753. https://doi.org/10.3390/tomography8020061

The primary script used to train the classifier is "train_multivariate_model.py". This file will train the classifiers and export ROC curves as shown in the paper.

The raw Radiomic feature data can be found in the "RadiomicFeatures" directory. 

The subdirectories in this folder correspond to the different image types from which the features were calculated.
The conventional energy-integrating features are in the subdirectory labelled "EID".
The photon counting ct features are in the subdirectory labelled "PCD".
The material basis decomposition features are in the subdirectory labelled "IPECS". These letters ("I", "PE", and "CS") correspond to the basis components which in this case are Iodine, the Photo-electric effect, and Compton Scattering.