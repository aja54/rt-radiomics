import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import auc


def analyze_training_results(y_actuals_list, y_predictions_list):
    list_lengths = len(y_actuals_list)
    number_of_thresholds = 50
    array_shapes = (list_lengths, number_of_thresholds)

    # Create arrays for storing all metrics:
    aucs = np.zeros(array_shapes)
    accuracies = np.zeros(array_shapes)
    false_positive_rates = np.zeros(array_shapes)
    true_positive_rates = np.zeros(array_shapes)
    precisions = np.zeros(array_shapes)

    for permutation in range(list_lengths):
        y_predictions = y_predictions_list[permutation]
        y_actuals = y_actuals_list[permutation]

        # Calculate false positives, true positives, etc....
        thresholds = np.linspace(0, 1, number_of_thresholds)
        fprs = np.zeros(thresholds.shape)
        tprs = np.zeros(thresholds.shape)
        precs = np.zeros(thresholds.shape)

        for loop_num, threshold in enumerate(thresholds):
            false_positives = 0
            true_positives = 0
            false_negatives = 0
            true_negatives = 0

            for y in range(y_predictions.shape[0]):
                y_pred_tmp = np.squeeze(y_predictions[y].copy())

                if y_pred_tmp >= threshold:
                    pred_val = 1
                else:
                    pred_val = 0

                if pred_val == 1 and np.squeeze(y_actuals)[y] == 0:
                    false_positives += 1
                elif pred_val == 1 and np.squeeze(y_actuals)[y] == 1:
                    true_positives += 1
                elif pred_val == 0 and np.squeeze(y_actuals)[y] == 0:
                    true_negatives += 1
                elif pred_val == 0 and np.squeeze(y_actuals)[y] == 1:
                    false_negatives += 1
                else:
                    print('Something has gone wrong. You shouldn\'t be here')

            denom_fpr = false_positives + true_negatives
            denom_tpr = true_positives + false_negatives
            denom_prec = true_positives + false_positives

            if denom_fpr == 0:
                fpr = 0
            else:
                fpr = false_positives / denom_fpr

            if denom_tpr == 0:
                tpr = 0
            else:
                tpr = true_positives / denom_tpr

            if denom_prec == 0:
                prec = 1
            else:
                prec = true_positives / denom_prec
                if prec == 0:
                    prec = 1

            precs[loop_num] = prec
            fprs[loop_num] = fpr
            tprs[loop_num] = tpr

        false_positive_rates[permutation, :] = fprs
        true_positive_rates[permutation, :] = tprs
        precisions[permutation, :] = precs

        # Compute AUC
        aucs[permutation, :] = auc(fprs, tprs)

        # Determine threshold used to calculate accuracy:
        threshold = determine_threshold(y_predictions, y_actuals)

        # Compute Accuracy
        y_actual_tmp = y_actuals
        y_pred_tmp = np.zeros(y_predictions.shape)
        y_pred_tmp[y_predictions >= threshold] = 1
        y_pred_tmp[y_predictions < threshold] = 0
        accuracy = np.sum(np.multiply(y_pred_tmp, y_actual_tmp)) / np.sum(y_actual_tmp)
        if np.isnan(accuracy):
            accuracy = 1.0
        accuracies[permutation, :] = accuracy

    # Create ROC
    averate_fprs = np.mean(false_positive_rates, axis=0)
    average_tprs = np.mean(true_positive_rates, axis=0)
    std_tprs = np.std(true_positive_rates, axis=0)
    top_tprs = average_tprs + std_tprs
    top_tprs[top_tprs > 1] = 1

    plt.rcParams.update({'font.size': 14})
    plt.figure(1)
    plt.clf()
    plt.figure(1)
    plt.plot(averate_fprs, average_tprs, 'b', alpha=0.8,
             label='ROC Curve')
    plt.fill_between(averate_fprs, top_tprs, average_tprs - std_tprs, alpha=0.1, label='+/- 1 std')
    plt.axis([-.1, 1.1, -.1, 1.1])
    plt.plot([0, 1], [0, 1], 'r--', alpha=0.3, label='Random Chance')
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.legend()
    ax = plt.gca()
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    plt.annotate(f'AUC: {np.mean(np.array(aucs)):.2f} +/- {np.std(np.array(aucs)):.2f}', (0.5, .5), ha='center',
                 bbox=dict(facecolor='white', edgecolor='black', boxstyle='round'))

    threshold_arg = np.argmin(np.abs(thresholds - threshold))
    print(f'Mean AUC: {np.mean(np.array(aucs)):.3f} +/- {np.std(np.array(aucs)):0.3f}')
    print(f'Mean Precision: {np.mean(precisions[:, threshold_arg]):.3f} +/- {np.std(precisions[:, threshold_arg]):0.3f}')
    print(f'Mean Recall: {np.mean(true_positive_rates[:, threshold_arg]):.3f} +/- {np.std(true_positive_rates[:, threshold_arg]):0.3f}')
    print(f'Mean Accuracy: {np.mean(np.array(accuracies)):.3f} +/- {np.std(np.array(accuracies)):0.3f}')


def determine_threshold(y_predicts, y_actuals):
    # Calculation of true/false positives/negatives:
    fprs = list()
    tprs = list()
    precs = list()
    thresholds = np.linspace(0, 1, 50)
    y_predictions = y_predicts.copy()
    for threshold in thresholds:
        false_positives = 0
        true_positives = 0
        false_negatives = 0
        true_negatives = 0

        for y in range(y_predictions.shape[0]):
            y_pred_tmp = np.squeeze(y_predictions[y])
            if y_pred_tmp >= threshold:
                y_pred_tmp = 1
            else:
                y_pred_tmp = 0
            if y_pred_tmp == 1 and np.squeeze(y_actuals)[y] == 0:
                false_positives += 1
            elif y_pred_tmp == 1 and np.squeeze(y_actuals)[y] == 1:
                true_positives += 1
            elif y_pred_tmp == 0 and np.squeeze(y_actuals)[y] == 0:
                true_negatives += 1
            elif y_pred_tmp == 0 and np.squeeze(y_actuals)[y] == 1:
                false_negatives += 1
            else:
                print('Something has gone wrong.')

        denom_fpr = false_positives + true_negatives
        denom_tpr = true_positives + false_negatives
        denom_prec = true_positives + false_positives

        if denom_fpr == 0:
            fpr = 0
        else:
            fpr = false_positives / denom_fpr

        if denom_tpr == 0:
            tpr = 0
        else:
            tpr = true_positives / denom_tpr

        if denom_prec == 0:
            prec = 1
        else:
            prec = true_positives / denom_prec
            if prec == 0:
                prec = 1

        precs.append(prec)
        fprs.append(fpr)
        tprs.append(tpr)

    fprs = np.array(fprs)
    tprs = np.array(tprs)
    precs = np.array(precs)

    intersect_threshold = thresholds[np.argmin(np.abs(tprs-precs))]
    return intersect_threshold
