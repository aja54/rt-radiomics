"""
Multivariate Analysis Script

This script allows for the training of our multivariate models using a repeated cross-validation strategy.
"""

import os
import csv
from glob import glob
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras import initializers
from sklearn.decomposition import PCA

import pymrmr

from analysis import analyze_training_results

# tf.config.set_visible_devices([], 'GPU')  # Include this line to avoid the use of GPU in classifier training.


def normalize_features(dataf):
    for col in dataf:
        mean = np.mean(dataf[col])
        std = np.std(dataf[col])
        min_ = np.min(dataf[col])
        max_ = np.max(dataf[col])

        if std != 0:
            dataf[col] = np.divide(dataf[col] - mean, std)
        else:
            dataf[col] = (dataf[col] - min_) / (max_ - min_)
    return dataf


radiomic_features_files = r".\RadiomicFeatures\*\radiomic_features.csv"

permutations = 50  # Number of times to repeat cross-validation folds
inner_folds = 5  # Number of inner folds that will be concatenated within each permutation.

# for num_features in range(1, 12): # This can be used to analyze the effect of varying the number of features chosen.
for num_features in range(1):  # Or you can choose to do one loop with a specific number of features to choose.
    if num_features == 0:
        num_features = 11  # (Specific number of chosen features)

    use_pca = False  # Alternately you can use PCA to determine the number of features.
    pca_component_percentage = 60

    """MAIN PROCESS OUTLINE:
    1. THE FULL TABLE OF FEATURES IS READ IN.
    2. CLASSIFICATION TRAINING IS PERFORMED (ON EACH IMAGE MODALITY)
    3. WITHIN EACH CLASSIFICATION STEP:
        a. We randomly remove 5 mice from the data (making sure both classes are represented)
        b. We then perform PCA to find a number of vectors needed to explain 90% of feature space of the remaining mice.
        c. We then reduce the feature set to only include the features selected 
           by MRMR for our subset of mice according to the number of vectors directed by PCA.
        d. We fit our model to the current feature set.
    4. AFTER EACH FOLD:
        a. We append these training results to a vector of network predictions.
    5. AFTER K FOLDS:
        a. Repeat the entire process with an entirely different combination choice.
    6. AFTER ALL COMBINATIONS:
        a. Get averages for all calculations and plot a statistical ROC curve
    """

    for file_count, file in enumerate(glob(radiomic_features_files)):
        if 'EID' in file or 'PCD' in file or 'IPECS' in file:
            folder = os.path.split(file)[0]

            # Create empty file for recording the names of the features which are selected:
            logging_file = folder + f'\\feature_selections.csv'
            with open(logging_file, 'w', newline='') as csvfile:
                pass

            # Create empty file for recording all validation predictions:
            results_file = folder + f'\\validation_results.csv'
            with open(results_file, 'w', newline='') as csvfile:
                pass

            # Read in Radiomic Features File and remove non-numeric information
            df = pd.read_csv(file)
            new_group = list()
            for val in df['Group']:
                if val == 'Rag2mm':
                    new_group.append(1)
                else:
                    new_group.append(0)
            df = df.drop(columns=['ID', 'Unnamed: 0', 'Group'])
            df = df.astype(np.float64)
            df = normalize_features(df)
            new_group = pd.Series(new_group, dtype=np.int32)
            df['Group'] = new_group
            cols = [df.columns[-1]] + [col for col in df if col != df.columns[-1]]
            df = df[cols]

            # Start outer loop:
            for outer_loop_number in range(permutations):
                # Keep track of a vector of results for each inner loop:
                y_val_list = list()
                y_pred_list = list()
                for inner_loop_number in range(inner_folds):

                    # Randomly select a subset of mice for testing
                    # (Being sure to maintain an even sampling of each class within each subset)
                    group1_indices = np.linspace(0, 11, 12, dtype=int)
                    group2_indices = np.linspace(12, 24, 13, dtype=int)
                    np.random.shuffle(group1_indices)
                    np.random.shuffle(group2_indices)
                    mice_removed = int(np.floor(25 / inner_folds))
                    idxs_to_remove = np.zeros((mice_removed,), dtype=int)
                    group = np.random.randint(0, 2)
                    group2_idx = 0
                    group1_idx = 0
                    for i in range(mice_removed):
                        if np.mod(group, 2) == 1:
                            idxs_to_remove[i] = group1_indices[group1_idx]
                            group1_idx += 1
                        if np.mod(group, 2) == 0:
                            idxs_to_remove[i] = group2_indices[group2_idx]
                            group2_idx += 1
                        group += 1

                    # Create training set using all but the removed mice:
                    df_subset = df
                    df_subset = df_subset.drop(index=idxs_to_remove, axis=0).reset_index(drop=True)
                    y_train = df_subset['Group'].to_numpy(dtype=np.float64)
                    x_train = list()

                    # Perform PCA Analysis to determine optimal feature number
                    if use_pca:
                        pca = PCA()
                        pca.fit(df_subset)
                        explained = 0
                        num_vectors = 0
                        for val in pca.explained_variance_ratio_:
                            explained += val
                            num_vectors += 1
                            if explained >= pca_component_percentage / 100:
                                print(f'PCA Vectors: {num_vectors}')
                                break
                    else:
                        num_vectors = num_features

                    # Perform MRMR to rank features and use them to define training set
                    features = pymrmr.mRMR(df_subset, 'MIQ', num_vectors)
                    with open(logging_file, 'a', newline='') as csvfile:  # Record the selected feature names
                        writer = csv.writer(csvfile, delimiter=',')
                        writer.writerow(features)
                    for feature in features:
                        x_train.append(df_subset[feature].to_numpy())
                    x_train = np.array(x_train)

                    # Create Validation set using the mice removed from the training set:
                    val_idxs = idxs_to_remove
                    x_val = list()
                    y_val = list()
                    for idx in val_idxs:
                        y_val.append(df['Group'].to_numpy(dtype=np.float64)[idx])
                        tmp_list = list()
                        for feature in features:
                            tmp_list.append(df[feature].to_numpy()[idx])
                        x_val.append(tmp_list)
                    y_val = np.array(y_val)
                    x_val = np.array(x_val)

                    # Define Network architecture
                    # LOGISTIC REGRESSION
                    model = Sequential()
                    seed = 5
                    model.add(Dense(1, input_dim=len(features), activation='sigmoid',
                                    kernel_initializer=initializers.RandomNormal(stddev=0.01, seed=seed),
                                    bias_initializer=initializers.Zeros()))
                    loss_object = tf.keras.losses.BinaryCrossentropy()
                    optimizer = tf.keras.optimizers.Adam(learning_rate=0.005)
                    model.compile(optimizer=optimizer, loss=loss_object,
                                  metrics=tf.keras.metrics.BinaryAccuracy(threshold=0.5))

                    title = file.split('\\')[-2]
                    x_train = x_train.swapaxes(0, 1)

                    print("Starting Training")
                    epoch_start = 0
                    # tb_callback = tf.keras.callbacks.TensorBoard( # You can monitor training using tensorboard
                    #     f'./logs/{title}/permutation{outer_loop_number}_fold{inner_loop_number}/')
                    stop_callback = tf.keras.callbacks.EarlyStopping(
                        monitor='val_loss', min_delta=0.001, patience=50, verbose=0,
                        mode='auto', baseline=None, restore_best_weights=False)
                    epochs = 250
                    history = model.fit(x_train, y_train, initial_epoch=epoch_start,
                                        epochs=epochs,
                                        batch_size=20,
                                        validation_data=(x_val, y_val), callbacks=[stop_callback])

                    # Make predictions and save in inner loop list:
                    y_pred = np.squeeze(model.predict(x_val))
                    y_val_list.append(y_val.copy())
                    y_pred_list.append(y_pred.copy())

                # Save validation predictions of each inner loop to the log file for final analysis:
                y_val_list = np.array(y_val_list).flatten()
                y_pred_list = np.array(y_pred_list).flatten()
                with open(results_file, 'a', newline='') as csvfile:
                    writer = csv.writer(csvfile, delimiter=',')
                    writer.writerow(y_val_list)
                    writer.writerow(y_pred_list)

            # -- END OF ALL LOOPS -- #
            # Read in full file of validation predictions and do some analysis:
            # Create ROC curve
            # Calculate accuracy, precision, recall, and auc

            validation_predictions = list()
            validation_labels = list()
            with open(results_file, 'r', newline='') as csvfile:
                reader = csv.reader(csvfile, delimiter=',')
                for num, row in enumerate(reader):
                    row = [float(val) for val in row]
                    if np.mod(num, 2) == 0:
                        validation_labels.append(row)
                    else:
                        validation_predictions.append(row)

            validation_predictions = np.array(validation_predictions)
            validation_labels = np.array(validation_labels)
            analyze_training_results(validation_labels, validation_predictions)

            plt.figure(1)
            plt.savefig(
                f'./Figure/ROC{title}.png',
                transparent=True)
